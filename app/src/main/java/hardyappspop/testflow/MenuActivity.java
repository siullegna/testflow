package hardyappspop.testflow;

import android.animation.Animator;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.squareup.otto.Subscribe;

import butterknife.ButterKnife;
import hardyappspop.testflow.enums.MenuItemType;
import hardyappspop.testflow.event.EventBus;
import hardyappspop.testflow.event.MenuItemClickEvent;
import hardyappspop.testflow.preference.MySharedPreference;
import hardyappspop.testflow.widget.MenuItem;

/**
 * Created by luis on 2/20/16.
 */
public class MenuActivity extends AppCompatActivity implements OnTouchListener {
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = MenuActivity.class.getName();
    // ===========================================================
    // Fields
    // ===========================================================
    private View viewContainer;
    private MenuItem menuCancel;
    private MenuItem menuActivity;
    private MenuItem menuMatches;
    private MenuItem menuComm;
    private MenuItem menuWhatIf;

    private boolean isAnimated = false;
    private boolean isCancel = false;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            EventBus.INSTANCE.getBus().register(this);
        } catch (IllegalStateException e) {
            Log.e(TAG, e.getMessage());
        }

        if (!isAnimated) {
            isAnimated = true;
            final int yOffset = -240;

            menuCancel.setVisibility(View.VISIBLE);
            menuCancel.animate().translationY(0);

            menuActivity.setVisibility(View.VISIBLE);
            menuActivity.animate().translationY(yOffset);

            menuMatches.setVisibility(View.VISIBLE);
            menuMatches.animate().translationY(2 * yOffset);

            menuComm.setVisibility(View.VISIBLE);
            menuComm.animate().translationY(3 * yOffset);

            menuWhatIf.setVisibility(View.VISIBLE);
            menuWhatIf.animate().translationY(4 * yOffset);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            EventBus.INSTANCE.getBus().unregister(this);
        } catch (IllegalStateException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v == viewContainer) {
            finish();
            return true;
        }
        return false;
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void init() {
        viewContainer = ButterKnife.findById(this, R.id.view_container);
        menuCancel = ButterKnife.findById(this, R.id.menu_cancel);
        menuActivity = ButterKnife.findById(this, R.id.menu_activity);
        menuMatches = ButterKnife.findById(this, R.id.menu_matches);
        menuComm = ButterKnife.findById(this, R.id.menu_comm);
        menuWhatIf = ButterKnife.findById(this, R.id.menu_what_if);

        setupInfo();

        viewContainer.setOnTouchListener(this);
    }

    private void setupInfo() {
        menuCancel.setTxtItemVisibility(View.GONE);
        menuCancel.setMenuItemType(MenuItemType.CANCEL);

        final Resources res = getResources();
        menuActivity.setTxtItem(res.getString(R.string.menu_activity));
        menuActivity.setMenuItemType(MenuItemType.ACTIVITY);

        menuMatches.setTxtItem(res.getString(R.string.menu_match));
        menuMatches.setMenuItemType(MenuItemType.MATCHES);

        menuComm.setTxtItem(res.getString(R.string.menu_comm));
        menuComm.setMenuItemType(MenuItemType.COMM);

        menuWhatIf.setTxtItem(res.getString(R.string.menu_what_if));
        menuWhatIf.setMenuItemType(MenuItemType.WHAT_IF);
    }

    private void makeAnimation() {
        menuActivity.animate().translationY(0);
        menuMatches.animate().translationY(0);
        menuComm.animate().translationY(0);
        menuWhatIf.animate().translationY(0);

        menuWhatIf.animate().setListener(animatorListener);
    }

    @Subscribe
    public void onClickListener(final MenuItemClickEvent menuItemClickEvent) {
        if (menuItemClickEvent != null && menuItemClickEvent.getMenuItemType() != null) {
            isCancel = false;
            makeAnimation();
            final MenuItemType menuItemType = menuItemClickEvent.getMenuItemType();
            Log.d(TAG, menuItemType.getValue());
            switch (menuItemType) {
                case ACTIVITY:
                    // go to Activity
                    break;
                case MATCHES:
                    // go to Matches
                    break;
                case COMM:
                    // go to Comm
                    break;
                case WHAT_IF:
                    // go to What if?
                    break;
                case CANCEL:
                    isCancel = true;
                    break;
            }
        }
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            menuActivity.setVisibility(View.GONE);
            menuMatches.setVisibility(View.GONE);
            menuComm.setVisibility(View.GONE);
            menuWhatIf.setVisibility(View.GONE);

            MySharedPreference mySharedPreference = new MySharedPreference(getApplicationContext());
            mySharedPreference.putBoolean(getApplicationContext().getResources().getString(R.string.sp_is_cancel),
                    isCancel);

            finish();
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };
}
