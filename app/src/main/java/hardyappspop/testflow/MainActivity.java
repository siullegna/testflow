package hardyappspop.testflow;

import android.animation.Animator;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;

import hardyappspop.testflow.adapter.ViewPagerAdapter;
import hardyappspop.testflow.animation.CustomAnimation;
import hardyappspop.testflow.fragment.ActivityFragment;
import hardyappspop.testflow.util.Constants;

public class MainActivity extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageButton activityFeedFloatButton;
    private boolean isShown = true;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        activityFeedFloatButton = (ImageButton) findViewById(R.id.activity_feed_float_button);

        activityFeedFloatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShown) {
                    activityFeedFloatButton.animate().translationY(800);
                    isShown = false;
                }
            }
        });

        activityFeedFloatButton.animate().setListener(onAnimatorListener);
    }

    /**
     * Dispatch onStart() to all fragments.  Ensure any created loaders are
     * now started.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    public void onResume() {
        super.onResume();
        if (!isShown) {
            activityFeedFloatButton.animate().translationY(0);
            isShown = true;
        }
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ActivityFragment(), Constants.ALL_UPDATES);
        adapter.addFragment(new ActivityFragment(), Constants.MESSAGES);
        adapter.addFragment(new ActivityFragment(), Constants.VISITORS);
        adapter.addFragment(new ActivityFragment(), Constants.PROFILE_UPDATES);
        adapter.addFragment(new ActivityFragment(), Constants.PHOTO_UPDATES);
        viewPager.setAdapter(adapter);
    }

    private void startActivityAnimation02() {
        Intent iMenu = new Intent(this, MenuActivity.class);
        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, activityFeedFloatButton, "");

        ActivityCompat.startActivity(this, iMenu, activityOptionsCompat.toBundle());
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private Animator.AnimatorListener onAnimatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (!isShown) {
                startActivityAnimation02();
            }
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };
}
