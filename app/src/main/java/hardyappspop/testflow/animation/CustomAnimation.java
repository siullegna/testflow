package hardyappspop.testflow.animation;

import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

/**
 * Created by luis on 2/20/16.
 */
public enum CustomAnimation {
    INSTANCE;

    /**
     * @param view      - view that we want to animate
     * @param toDegrees - where to stop the animation
     * @param duration  - the time that the animation should takes fo finishes a cycle
     */
    public RotateAnimation rotateAnimation(final View view, final float toDegrees, final int duration) {
        final float fromDegrees = 0.0f;
        final int repeatCount = 0;
        return rotateAnimation(fromDegrees, toDegrees, repeatCount, duration, view.getPivotX(), view.getPivotY());
    }

    /**
     * @param fromDegrees - where to start the animation
     * @param toDegrees   - where to stop the animation
     * @param repeatCount - how many times we want to repeat the animation before we stop that
     * @param duration    - the time that the animation should takes fo finishes a cycle
     * @param pivotX      -
     * @param pivotY      -
     * @return {@link RotateAnimation} - the custom animation based on the values that
     * are coming from the params
     */
    private RotateAnimation rotateAnimation(final float fromDegrees, final float toDegrees,
                                            final int repeatCount, final int duration, final float pivotX,
                                            final float pivotY) {
        final RotateAnimation anim = new RotateAnimation(fromDegrees, toDegrees, pivotX, pivotY);

        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(repeatCount);
        anim.setDuration(duration);

        return anim;
    }
}
