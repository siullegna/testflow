package hardyappspop.testflow.preference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by luis on 2/20/16.
 */
public class MySharedPreference {
    // ===========================================================
    // Constants
    // ===========================================================
    private final String SHARED_PREFERENCES = "mysharedpreference.testflow";
    // ===========================================================
    // Fields
    // ===========================================================
    private SharedPreferences sPreferences;
    private SharedPreferences.Editor sEditor;

    // ===========================================================
    // Constructors
    // ===========================================================
    public MySharedPreference(Context context) {
        sPreferences = context.getSharedPreferences(this.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        sEditor = sPreferences.edit();
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    /**
     *
     */
    public void putString(String key, String defValue) {
        sEditor.putString(key, defValue);
        sEditor.commit();
    }

    /**
     *
     */
    public String getString(String key, String defValue) {
        return sPreferences.getString(key, defValue);
    }

    /**
     *
     */
    public void putInteger(String key, Integer defValue) {
        sEditor.putInt(key, defValue);
        sEditor.commit();
    }

    /**
     *
     */
    public Integer getInteger(String key, Integer defValue) {
        return sPreferences.getInt(key, defValue);
    }

    public void putBoolean(String key, boolean defValue) {
        sEditor.putBoolean(key, defValue);
        sEditor.commit();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return sPreferences.getBoolean(key, defValue);
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
