package hardyappspop.testflow.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import hardyappspop.testflow.R;
import hardyappspop.testflow.enums.MenuItemType;
import hardyappspop.testflow.event.EventBus;
import hardyappspop.testflow.event.MenuItemClickEvent;

/**
 * Created by luis on 2/20/16.
 */
public class MenuItem extends LinearLayout implements View.OnClickListener {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private TextView txtItem;
    private ImageButton imgButton;
    private MenuItemType menuItemType;
    // ===========================================================
    // Constructors
    // ===========================================================
    public MenuItem(Context context) {
        this(context, null);
    }

    public MenuItem(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.menu_item, this);
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================
    public void setTxtItem(final String text) {
        txtItem.setText(text);
    }

    public void setImgButton(final int iconId) {
        imgButton.setBackgroundResource(iconId);
    }

    public void setTxtItemVisibility(final int visibility) {
        txtItem.setVisibility(visibility);
    }

    public void setMenuItemType(MenuItemType menuItemType) {
        this.menuItemType = menuItemType;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    /**
     * Finalize inflating a view from XML.  This is called as the last phase
     * of inflation, after all child views have been added.
     * <p/>
     * <p>Even if the subclass overrides onFinishInflate, they should always be
     * sure to call the super method, so that we get called.
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        txtItem = ButterKnife.findById(this, R.id.txt_item);
        imgButton = ButterKnife.findById(this, R.id.img_item);

        imgButton.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        EventBus.INSTANCE.getBus().post(new MenuItemClickEvent(menuItemType));
    }
    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
