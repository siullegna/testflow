package hardyappspop.testflow.enums;

/**
 * Created by luis on 2/20/16.
 */
public enum MenuItemType {
    ACTIVITY("activity"),
    MATCHES("matches"),
    COMM("comm"),
    WHAT_IF("what if?"),
    CANCEL("cancel");
    private final String value;

    MenuItemType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
