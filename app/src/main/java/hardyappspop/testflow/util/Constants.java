package hardyappspop.testflow.util;

/**
 * Created by luis on 2/19/16.
 */
public class Constants {
    public static final String ALL_UPDATES = "ALL UPDATES";
    public static final String MESSAGES = "MESSAGES";
    public static final String VISITORS = "VISITORS";
    public static final String PROFILE_UPDATES = "PROFILE UPDATES";
    public static final String PHOTO_UPDATES = "PHOTO UPDATES";
}
