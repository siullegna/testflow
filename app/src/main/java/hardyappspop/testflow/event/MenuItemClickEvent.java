package hardyappspop.testflow.event;

import hardyappspop.testflow.enums.MenuItemType;

/**
 * Created by luis on 2/20/16.
 */
public class MenuItemClickEvent {
    private final MenuItemType menuItemType;

    public MenuItemClickEvent(MenuItemType menuItemType) {
        this.menuItemType = menuItemType;
    }

    public MenuItemType getMenuItemType() {
        return menuItemType;
    }
}
