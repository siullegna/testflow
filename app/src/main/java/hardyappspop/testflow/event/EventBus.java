package hardyappspop.testflow.event;

import com.squareup.otto.Bus;

/**
 * Created by luis on 2/20/16.
 */
public enum EventBus {
    INSTANCE;
    private final Bus bus = new Bus();

    public Bus getBus() {
        return bus;
    }
}
